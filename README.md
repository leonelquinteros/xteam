## Overview

This application is distributed as a zip file with all the source code needed to run.

Also, distributed with the code are 3 pre-compiled binaries in the `bin` directory. These are for amd64 processor architectures and for Windows, Linux and MacOS operating systems. These binaries are compiled from a MacOS amd64 workstation using Go's cross compilation features. 
 
Despite that, as it's developed using the standard Go guidelines to develop Go software described in [https://golang.org/doc/code.html](https://golang.org/doc/code.html), it can be installed using Go tools as described in the Installation section below. 


## Compilation

```
$ cd /path/to/unzipped/xteam
$ go build .
```


## Installation

```
$ go install bitbucket.org/leonelquinteros/xteam
```


## Run

The compiled binary doesn't needs any external file to run, so it can be deployed by itself to any environment.

On a typical Go environment setup, where `$GOPATH/bin` is added to the system `PATH` you can just:

```
$ xteam
```

Otherwise: 

```
$ /path/to/xteam
```


## Requirements

- A running and available Memcached server instance. 
- A running and available X-Team `daw-purchases` API server instance.


## Configuration

The application can be configured using environment variables. All variables are optional and have a default value as described below: 


- **XTEAM_HOST**: The host/domain name where the HTTP server listen for connections. It can be empty to listen on any interface/host. Default: ""

- **XTEAM_PORT**: The port where the HTTP server listen for connections. Default: "8001"

- **XTEAM_MEMCACHED_HOST**: The port where the HTTP server listen for connections. Default: "localhost:11211"

- **XTEAM_MEMCACHED_PREFIX**: The key prefix to use in front of all memcache keys. Default: "x-team_"

- **XTEAM_DATA_API_HOST**: The host where the x-team data API is running. Default: "http://localhost:8000"


### Runtime configuration example:

```
$ XTEAM_PORT=3333 XTEAM_MEMCACHED_HOST=10.8.0.11:11211 XTEAM_DATA_API_HOST=10.8.0.15:8080 /path/to/xteam 
```



## Best practices

This application is made following some best practices described below.
 

### Testing

Multiple tests are written for this application. From Unit Tests to Integration tests trying to take the test coverage as close as possible to 100%.

To execute all tests with coverage calculation and the race detector, run: 

```
$ cd /path/to/root/of/xteam
$ go test -cover -race ./...
```


### Caching

This application uses a Memcached service to cache all external data requests so further requests are faster. 
Items in cache are stored for 10 minutes and as there is no way to invalidate the data nor further specs about this, I've just set a random amount of time. 


### Directories/packages structure

The application, even when it's small and it could be all written in the main package, is organized into several directories: 

- `/` contains the main execution point and the minimum amount of routines needed to start the web server and application
- `/app` wraps application specific logic. 
- `/app/controllers` contains the (yet) only controller needed for this application. A controller in this context is named after the MVC pattern, even when we're not using an MVC architecture, the concept for the controller is pretty much the same and well known.
- `/app/models` follows the same idea from controllers. It contains the data models for the application. 
- `lib` contains utility packages such as the cache or the data access packages. 
- `vendor` is the standard Go vendor folder where all external dependencies are stored and versioned. 


### 12 factor apps

The overall architecture and design is modeled after the 12 factor manifest [https://12factor.net/](https://12factor.net/) to be portable and scalable. Below are the 12 factor implementation explained.


**I. Codebase**

This application is using Git for code versioning and available from a Bitbucket.org repository. The entire codebase can be retrieved from the repository and automatic deploys can be made. 


**II. Dependencies**

Dependencies for this application are vendored using the Go dep tool and following the language standards, so no external dependencies are needed.


**III. Config**

All possible configuration values for the application can be provided via environment variables.


**IV. Backing services**

The 2 backing services for this application are the data API providede by X-Team and the Memcached server required to cache data. Access information to both services can be provided using environment variables and both have default values to run locally. 


**V. Build, release, run**

This factor doesn't applies for this example application, but it's still possible without changes. 


**VI. Processes**

The application is stateless and shares nothing, so it can be distributed into many instances for load balancing without changes.


**VII. Port binding**

The server port for the application can be configured using the `PORT` environment variable


**VIII. Concurrency**

As any standard Go net/http based application, every request is handled by a separated goroutine.
Also, the `RecentPurchases.Get()` controller method in `app/controllers/recent_purchases`, retrieves the data from the API in a concurrent way so multiple requests to the data API are made at the same time without having to wait for the previous one to complete.


**IX. Disposability**

Go applications and specially this one are fast to start, so the process can be restarted and replaced without changes. 
Graceful shutdown isn't implemented, but no critical processes can damage any data when shutting down this server. 


**X. Dev/prod parity**

Development and Production environments can be switched by changing the values for the configuration variables. The code is the same.


**XI. Logs**

Logs are sent to the standard output so it can be treated as a stream or handled by Unix utilities.


**XII. Admin processes**

There is no admin processes for this application


