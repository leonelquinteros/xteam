package controllers

import (
	"bitbucket.org/leonelquinteros/xteam/app/models"
	"github.com/yarf-framework/yarf"
	"sync"
)

// RecentPurchasesResponse is the expected data format for RecentPurchases controller.
type RecentPurchasesResponse struct {
	models.Product
	Recent []string `json:"recent"`
}

// RecentPurchases is a controller for /api/recent_purchases/:username
type RecentPurchases struct {
	yarf.Resource
}

// Get is the HTTP GET method handler for this controller.
func (rp *RecentPurchases) Get(ctx *yarf.Context) error {
	// Get user first
	u, err := models.GetUser(ctx.Param("username"))
	if err != nil {
		ctx.Status(400)
		ctx.Render(err.Error())
		return err
	}

	// Get last 5 purchases
	ps, err := models.GetPurchasesByUser(u.Username, 5)
	if err != nil {
		ctx.Status(400)
		ctx.Render(err.Error())
		return err
	}

	// Init response
	r := make([]RecentPurchasesResponse, 0)
	rc := make(chan RecentPurchasesResponse)
	done := make(chan bool)

	// Get concurrent data and generate response.
	go rp.createResponse(ps, rc, done)

	for {
		select {
		case rpr := <-rc:
			// Skip repeated products
			repeated := false
			for i := range r {
				if rpr.Id == r[i].Id {
					repeated = true
					break
				}
			}

			// Insert ordered
			if !repeated {
				flag := true
				for i := range r {
					if len(rpr.Recent) > len(r[i].Recent) {
						r = append(r[:i], append([]RecentPurchasesResponse{rpr}, r[i:]...)...)
						flag = false
						break
					}
				}
				if flag {
					r = append(r, rpr)
				}
			}

		case <-done:
			// Render response and return
			ctx.RenderJSON(r)
			return nil
		}
	}
}

// createResponse handles concurrent data access to fill response items.
func (rp *RecentPurchases) createResponse(ps []models.Purchase, rc chan RecentPurchasesResponse, done chan bool) {
	// Sync concurrent calls
	var wg sync.WaitGroup

	// Get product info and other customers
	for _, p := range ps {
		wg.Add(1)

		go func(purchase models.Purchase, rc chan RecentPurchasesResponse) {
			defer wg.Done()

			// Product info
			prod, err := models.GetProduct(purchase.ProductId)
			if err != nil {
				return
			}

			// Create response object
			rpr := RecentPurchasesResponse{
				Product: prod,
			}

			// Other customers
			cs, err := models.GetPurchasesByProduct(purchase.ProductId)
			if err != nil {
				return
			}

			// Fill recent purchases
			rpr.Recent = make([]string, len(cs))
			for i, c := range cs {
				rpr.Recent[i] = c.Username
			}

			// Send item to channel
			rc <- rpr
		}(p, rc)
	}

	// Wait for all goroutines to finish
	wg.Wait()

	// Done
	done <- true
}
