package controllers

import (
	"bitbucket.org/leonelquinteros/xteam/app/models"
	"github.com/yarf-framework/yarf"
	"net/http/httptest"
	"testing"
)

func TestRecentPurchases(t *testing.T) {
	// Get first user
	us, err := models.GetUsers(1)
	if err != nil {
		t.Error(err.Error())
	}

	if len(us) == 0 {
		t.Fatal("Can't retrieve user. Check data API")
	}

	// Prepare request context
	r := httptest.NewRequest("GET", "/api/recent_purchases/"+us[0].Username, nil)
	rw := httptest.NewRecorder()

	ctx := yarf.NewContext(r, rw)
	ctx.Params.Set("username", us[0].Username)

	// Create controller
	rp := new(RecentPurchases)
	err = rp.Get(ctx)
	if err != nil {
		t.Error(err.Error())
	}
}

func TestInvalidUser(t *testing.T) {
	// Prepare request context
	r := httptest.NewRequest("GET", "/api/recent_purchases/garbage999888777", nil)
	rw := httptest.NewRecorder()

	ctx := yarf.NewContext(r, rw)
	ctx.Params.Set("username", "garbage999888777")

	// Create controller
	rp := new(RecentPurchases)
	err := rp.Get(ctx)
	if err == nil {
		t.Error("Invalid user should have returned an error")
	}
}

func TestRace(t *testing.T) {
	for i := 0; i < 100; i++ {
		go TestRecentPurchases(t)
	}
}
