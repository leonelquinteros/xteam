package models

import (
	"bitbucket.org/leonelquinteros/xteam/lib/data"
	"errors"
	"strconv"
)

// Product type represents product objects from the data API
type Product struct {
	Id    int    `json:"id"`
	Face  string `json:"face"`
	Size  int    `json:"size"`
	Price int    `json:"price"`
}

// GetProduct retrieves a product object by id
func GetProduct(productId int) (r Product, err error) {
	p := struct {
		Product Product
	}{}

	err = data.GetDataFrom("/api/products/"+strconv.Itoa(productId), &p)
	if err == nil {
		r = p.Product
		if r.Id != productId || r.Id == 0 {
			err = errors.New("Product with id of '" + strconv.Itoa(productId) + "' was not found")
		}
	}

	return
}

// GetProducts retrieves users collection
func GetProducts(limit int) (r []Product, err error) {
	ps := struct {
		Products []Product
	}{}
	err = data.GetDataFrom("/api/products?limit="+strconv.Itoa(limit), &ps)
	if err == nil {
		r = ps.Products
	}

	return
}
