package models

import (
	"testing"
)

func TestGetInvalidProduct(t *testing.T) {
	_, err := GetProduct(0)
	if err == nil {
		t.Error("Product ID '0' should have returned an error")
	}
}

func TestGetProducts(t *testing.T) {
	ps, err := GetProducts(1)
	if err != nil {
		t.Error(err.Error())
	}

	// Test get single product if possible
	if len(ps) > 0 {
		p, err := GetProduct(ps[0].Id)
		if err != nil {
			t.Error(err.Error())
		}

		if p.Id != ps[0].Id {
			t.Errorf("Retrieved products should be equal to the first from the list. '%d' vs '%d'", p.Id, ps[0].Id)
		}
	} else {
		t.Error("Got empty products list. Check data service.")
	}
}
