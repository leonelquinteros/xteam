package models

import (
	"bitbucket.org/leonelquinteros/xteam/lib/data"
	"strconv"
	"time"
)

// Purchase type represent purchase objects from the data API
type Purchase struct {
	Id        int       `json:"id"`
	ProductId int       `json:"productId"`
	Username  string    `json:"username"`
	Date      time.Time `json:"date"`
}

// GetPurchasesByUser retrieves the purchases made by a user.
func GetPurchasesByUser(username string, limit int) (r []Purchase, err error) {
	ps := struct {
		Purchases []Purchase
	}{}

	err = data.GetDataFrom("/api/purchases/by_user/"+username+"?limit="+strconv.Itoa(limit), &ps)
	if err == nil {
		r = ps.Purchases
	}

	return
}

// GetPurchasesByProduct retrieves purchases by product.
func GetPurchasesByProduct(productId int) (r []Purchase, err error) {
	ps := struct {
		Purchases []Purchase
	}{}

	err = data.GetDataFrom("/api/purchases/by_product/"+strconv.Itoa(productId), &ps)
	if err == nil {
		r = ps.Purchases
	}

	return
}
