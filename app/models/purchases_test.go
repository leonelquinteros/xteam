package models

import (
	"testing"
)

func TestGetPurchases(t *testing.T) {
	us, err := GetUsers(1)
	if err != nil {
		t.Error(err.Error())
	}

	// Test get single user if possible
	if len(us) > 0 {
		ps, err := GetPurchasesByUser(us[0].Username, 1)
		if err != nil {
			t.Error(err.Error())
		}

		if len(ps) > 0 {
			if ps[0].Username != us[0].Username {
				t.Fatalf("Retrieved username should be equal to the first from the list. '%s' vs '%s'", ps[0].Username, us[0].Username)
			}

			ps2, err := GetPurchasesByProduct(ps[0].ProductId)
			if err != nil {
				t.Error(err.Error())
			}

			if len(ps2) > 0 {
				if ps2[0].ProductId != ps[0].ProductId {
					t.Fatalf("Retrieved product id should be equal to the first from the list. '%d' vs '%d'", ps2[0].ProductId, ps[0].ProductId)
				}
			}
		}
	} else {
		t.Error("Got empty users list. Check data service.")
	}
}
