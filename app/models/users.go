package models

import (
	"bitbucket.org/leonelquinteros/xteam/lib/data"
	"errors"
	"strconv"
)

// User type represents user objects from the API
type User struct {
	Username string `json:"username"`
	Email    string `json:"email"`
}

// GetUser retrieves a user object by its username
func GetUser(username string) (r User, err error) {
	u := struct {
		User User
	}{}

	err = data.GetDataFrom("/api/users/"+username, &u)
	if err == nil {
		r = u.User
		if r.Username != username {
			err = errors.New("User with username of '" + username + "' was not found")
		}
	}

	return
}

// GetUsers retrieves users collection
func GetUsers(limit int) (r []User, err error) {
	us := struct {
		Users []User
	}{}
	err = data.GetDataFrom("/api/users?limit="+strconv.Itoa(limit), &us)
	if err == nil {
		r = us.Users
	}

	return
}
