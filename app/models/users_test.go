package models

import (
	"testing"
)

func TestGetInvalidUser(t *testing.T) {
	_, err := GetUser("nonexistentuser")
	if err == nil {
		t.Error("'nonexistentuser' should have returned an error")
	}
}

func TestGetUsers(t *testing.T) {
	us, err := GetUsers(10)
	if err != nil {
		t.Error(err.Error())
	}

	// Test get single user if possible
	if len(us) > 0 {
		u, err := GetUser(us[0].Username)
		if err != nil {
			t.Error(err.Error())
		}

		if u.Username != us[0].Username {
			t.Errorf("Retrieved username should be equal to the first from the list. '%s' vs '%s'", u.Username, us[0].Username)
		}
	} else {
		t.Error("Got empty users list. Check data service.")
	}
}
