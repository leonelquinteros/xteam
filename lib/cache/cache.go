// Package cache wraps memcache client functionality
// and handles cache key prefix to make it transparent for the consumer.
package cache

import (
	"crypto/sha512"
	"encoding/hex"
	"errors"
	"github.com/bradfitz/gomemcache/memcache"
	"os"
)

// cache type wraps the client and the configuration.
type cache struct {
	client *memcache.Client
	prefix string
}

// New creates a new cache client wrapper using the configuration from the environment variables.
func New() *cache {
	host := os.Getenv("XTEAM_MEMCACHED_HOST")
	if host == "" {
		host = "localhost:11211"
	}

	prefix := os.Getenv("XTEAM_MEMCACHED_PREFIX")
	if prefix == "" {
		prefix = "x-team_"
	}

	r := new(cache)
	r.client = memcache.New(host)
	r.prefix = prefix

	return r
}

// Key generates a hex-encoded SHA512 hash string from the desired key and the client prefix.
func (c *cache) Key(key string) string {
	sha := sha512.Sum512([]byte(c.prefix + key))
	return hex.EncodeToString(sha[:])
}

// Get retrieves a cached item by its key.
func (c *cache) Get(key string) ([]byte, error) {
	item, err := c.client.Get(c.Key(key))
	if err != nil {
		return nil, errors.New(err.Error())
	}

	return item.Value, nil
}

// Set writes an item to the cache.
func (c *cache) Set(key string, value []byte, expiration int32) error {
	return c.client.Set(&memcache.Item{
		Key:        c.Key(key),
		Value:      value,
		Expiration: expiration,
	})
}

// Delete removes an item from the cache.
func (c *cache) Delete(key string) error {
	return c.client.Delete(c.Key(key))
}
