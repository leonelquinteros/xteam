package cache

import (
	"bytes"
	"github.com/bradfitz/gomemcache/memcache"
	"github.com/leonelquinteros/gorand"
	"testing"
)

func TestDefaultSettings(t *testing.T) {
	c := New()
	if c.prefix != "x-team_" {
		t.Errorf("Expected default prefix to be 'x-team_', got '%s' instead", c.prefix)
	}
}

func TestSetGetDelete(t *testing.T) {
	// Generate random data
	key, err := gorand.GetAlphaNumString(23)
	if err != nil {
		t.Fatal(err.Error())
	}

	data, err := gorand.GetBytes(256)
	if err != nil {
		t.Fatal(err.Error())
	}

	// Create client and set data
	c := New()
	err = c.Set(key, data, 60)
	if err != nil {
		t.Fatal(err.Error())
	}

	// Get data and compare
	rdata, err := c.Get(key)
	if err != nil {
		t.Fatal(err.Error())
	}
	if !bytes.Equal(rdata, data) {
		t.Errorf("Retrieved data 'rdata' was expected to be the same as generated 'data'.")
	}

	// Delete data and check
	err = c.Delete(key)
	if err != nil {
		t.Fatal(err.Error())
	}
	rdata, err = c.Get(key)
	if err.Error() != memcache.ErrCacheMiss.Error() {
		t.Fatal(err.Error())
	}
}

func TestRace(t *testing.T) {
	for i := 0; i < 100; i++ {
		go TestSetGetDelete(t)
	}
}
