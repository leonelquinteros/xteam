// data package centralizes data access to the x-team API
package data

import (
	"bitbucket.org/leonelquinteros/xteam/lib/cache"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// GetDataFrom takes a API method path and sets &obj with the JSON decoded data returned from it.
// Caches results for all responses up to 10 minutes.
func GetDataFrom(method string, obj interface{}) (err error) {
	// First try to get from cache
	check := getDataFromCache(method, &obj)
	if check == nil {
		// Found in cache.
		return
	}

	// Get from API
	r, err := http.Get(getUrlFor(method))
	defer r.Body.Close()
	if r.StatusCode != 200 {
		err = errors.New(fmt.Sprintf("%s: Error response with status %d - %s", r.Request.URL.String(), r.StatusCode, r.Status))
		return
	}

	// Read body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}

	// JSON decode
	err = json.Unmarshal(body, &obj)
	// Save result to cache
	if err != nil {
		err = setDataToCache(method, obj)
	}

	return
}

// getUrlFor returns the full API endpoint for a given method path.
func getUrlFor(method string) string {
	// Get from env or set default
	host := os.Getenv("XTEAM_DATA_API_HOST")
	if host == "" {
		host = "http://localhost:8000"
	}

	// Remove trailing slashes
	for host[len(host)-1] == '/' {
		host = host[:len(host)-1]
	}

	// Add initial slash
	if method != "" && method[0] != '/' {
		method = "/" + method
	}

	return host + method
}

// getDataFromCache tries to retrieve and decode an object from the cache.
func getDataFromCache(method string, obj interface{}) (err error) {
	// Get from cache
	c := cache.New()
	d, err := c.Get(getUrlFor(method))
	if err != nil {
		return
	}

	// Decode
	err = json.Unmarshal(d, &obj)
	return
}

// setDataToCache writes an object serialized to cache
func setDataToCache(method string, obj interface{}) (err error) {
	// Encode
	d, err := json.Marshal(&obj)
	if err != nil {
		return
	}

	// Set to cache for 10 minutes
	c := cache.New()
	err = c.Set(getUrlFor(method), d, 600)
	return
}
