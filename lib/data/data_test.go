package data

import (
	"reflect"
	"testing"
)

type DataTest struct {
	A string
	B int
}

func TestGetUrlFor(t *testing.T) {
	url := getUrlFor("")
	if url != "http://localhost:8000" {
		t.Fatalf("Expected default host to be 'http://localhost:8000'. Got '%s' instead", url)
	}

	url = getUrlFor("/some/method")
	if url != "http://localhost:8000/some/method" {
		t.Fatalf("Expected default host to be 'http://localhost:8000/some/method'. Got '%s' instead", url)
	}

	url = getUrlFor("some/method")
	if url != "http://localhost:8000/some/method" {
		t.Fatalf("Expected default host to be 'http://localhost:8000/some/method'. Got '%s' instead", url)
	}
}

func TestCache(t *testing.T) {
	data := DataTest{
		A: "Test data",
		B: 12345,
	}

	err := setDataToCache("/test/method", data)
	if err != nil {
		t.Fatal(err.Error())
	}

	data2 := DataTest{}
	err = getDataFromCache("/test/method", &data2)
	if err != nil {
		t.Fatal(err.Error())
	}

	if !reflect.DeepEqual(data, data2) {
		t.Errorf("Retrieved data from cache is different than data set. %v %v", data, data2)
	}
}

func TestGetDataFrom(t *testing.T) {
	data := make(map[string]interface{})

	// Check valid API method
	err := GetDataFrom("/api/users", &data)
	if err != nil {
		t.Error(err.Error())
	}

	// Check invalid api method
	err = GetDataFrom("/api/non/existent/method/", &data)
	if err == nil {
		t.Error("/api/non/existent/method/ should have returned an error.")
	}
}
