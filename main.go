package main

import (
	"bitbucket.org/leonelquinteros/xteam/app/controllers"
	"github.com/yarf-framework/extras/ratelimit"
	"github.com/yarf-framework/yarf"
	"log"
	"os"
	"runtime"
)

func main() {
	// Create a new empty YARF server
	y := yarf.New()

	// Rate limits per client
	y.Insert(ratelimit.YarfMiddleware(600, 60))

	// Log to stdout
	y.Logger = log.New(os.Stdout, "", log.Ldate|log.Ltime)
	y.Logger.Println("Starting server...")

	// Log panics and keep running
	y.PanicHandler = func() {
		if err := recover(); err != nil {
			// Get stack trace
			stack := make([]byte, 1<<16)
			runtime.Stack(stack, false)

			// Log panic
			y.Logger.Printf("PANIC! \n%v", err)
			y.Logger.Print(string(stack))
		}
	}

	// Route
	y.Add("/api/recent_purchases/:username", new(controllers.RecentPurchases))

	// Get HTTP host and port from environment or set default.
	host := os.Getenv("XTEAM_HOST") // Empty host is allowed
	port := os.Getenv("XTEAM_PORT")
	if port == "" {
		port = "8001"
	}
	y.Logger.Printf("Listening to %s:%s", host, port)

	// Run server
	y.Start(host + ":" + port)
}
